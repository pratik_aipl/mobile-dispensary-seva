package com.addit.bjsmobiledispensaryseva.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.addit.bjsmobiledispensaryseva.BaseActivity;
import com.addit.bjsmobiledispensaryseva.R;
import com.addit.bjsmobiledispensaryseva.listner.DialogButtonListener;
import com.addit.bjsmobiledispensaryseva.model.AmbulanceModel;
import com.addit.bjsmobiledispensaryseva.model.DrModel;
import com.addit.bjsmobiledispensaryseva.model.SexModel;
import com.addit.bjsmobiledispensaryseva.network.NetworkRequest;
import com.addit.bjsmobiledispensaryseva.utils.Constant;
import com.addit.bjsmobiledispensaryseva.utils.L;
import com.addit.bjsmobiledispensaryseva.utils.Validation;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;

public class FromFillActivity extends BaseActivity {

    private static final String TAG = "FromFillActivity";
    @BindView(R.id.img_back)
    ImageView imgBack;
    @BindView(R.id.tv_tittle)
    TextView tvTittle;
    @BindView(R.id.img_filter)
    ImageView imgFilter;
    @BindView(R.id.lli)
    LinearLayout lli;
    @BindView(R.id.edt_fname)
    EditText edtFname;
    @BindView(R.id.edt_Lname)
    EditText edtLname;
    @BindView(R.id.edt_mobile)
    EditText edtMobile;
    @BindView(R.id.spn_gender)
    Spinner spnGender;
    @BindView(R.id.li_gender)
    LinearLayout liGender;
    @BindView(R.id.edt_age)
    EditText edtAge;
    @BindView(R.id.spn_drname)
    Spinner spnDrname;
    @BindView(R.id.li_drname)
    LinearLayout liDrname;
    @BindView(R.id.spn_dAmbulanceNo)
    Spinner spnDAmbulanceNo;
    @BindView(R.id.li_AmbulanceNo)
    LinearLayout liAmbulanceNo;
    @BindView(R.id.edt_Area)
    EditText edtArea;
    @BindView(R.id.edt_zipcode)
    EditText edtZipcode;
    @BindView(R.id.spn_Csuspect)
    Spinner spnCsuspect;
    @BindView(R.id.li_Csuspect)
    LinearLayout liCsuspect;
    @BindView(R.id.edt_remark)
    EditText edtRemark;
    @BindView(R.id.edt_email)
    EditText edtemail;
    @BindView(R.id.btn_submit)
    Button btnSubmit;

    String gender,CSuspectStatus,DRID,AMBULANCEID;

    List<SexModel> sex = new ArrayList<>();
    SexAdapter sexAdapter;

    List<SexModel> CSuspectList = new ArrayList<>();
    CSuspect cSuspect;

    List<DrModel> DrList = new ArrayList<>();
    DRListAdapter drListAdapter;

    List<AmbulanceModel> ambulanceList = new ArrayList<>();
    AmbulanceListAdapter ambulanceListAdapter;

    Subscription subscription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_from_fill);
        ButterKnife.bind(this);
        tvTittle.setText("ENTRY FORM");
        imgFilter.setVisibility(View.VISIBLE);

        sex.add(new SexModel("Male", 1));
        sex.add(new SexModel("Female", 2));
        sex.add(new SexModel("Gender", 3));

        sexAdapter = new SexAdapter(this, sex);
        spnGender.setAdapter(sexAdapter);

        CSuspectList.add(new SexModel("Low", 1));
        CSuspectList.add(new SexModel("Medium", 2));
        CSuspectList.add(new SexModel("High", 3));
        CSuspectList.add(new SexModel("Corona Suspect", 4));

        cSuspect = new CSuspect(this, CSuspectList);
        spnCsuspect.setAdapter(cSuspect);

        drListAdapter = new DRListAdapter(this, DrList);
        spnDrname.setAdapter(drListAdapter);

        ambulanceListAdapter = new AmbulanceListAdapter(this, ambulanceList);
        spnDAmbulanceNo.setAdapter(ambulanceListAdapter);

        if (L.isNetworkAvailable(FromFillActivity.this))
            getDrList();
        spnDrname.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0)
                    DRID = String.valueOf(DrList.get(position - 1).getDoctorID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spnDAmbulanceNo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position != 0)
                    AMBULANCEID = String.valueOf(ambulanceList.get(position - 1).getAmbulanceID());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }
    @OnClick({R.id.btn_submit, R.id.img_back,R.id.img_filter})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                validation();
                break;
            case R.id.img_back:
                onBackPressed();
                break;
                case R.id.img_filter:
                    Alert();
                break;
        }
    }

    public void validation() {
        if (Validation.isEmpty(L.getEditText(edtFname))) {
            edtFname.setError("Please enter First Name");
        } else if ((L.getEditText(edtFname).length() < 3)) {
            edtFname.setError("Please enter Minimum 3 Character");
        } else if (Validation.isEmpty(L.getEditText(edtLname))) {
            edtLname.setError("Please enter Last Name");
        } else if ((L.getEditText(edtLname).length() < 3)) {
            edtLname.setError("Please enter Minimum 3 Character");
        } else if (!Validation.isValidPhoneNumber(edtMobile.getText())) {
            edtMobile.setError("Check Mobile No");
        }else if (sex.size() == 0 || spnGender.getSelectedItemPosition() == 0) {
            Toast.makeText(this, "Please Select Gender", Toast.LENGTH_SHORT).show();
        } else if (Validation.isEmpty(L.getEditText(edtAge))) {
            edtAge.setError("Please enter Age");
        }else {
            if (L.isNetworkAvailable(FromFillActivity.this)) {
                if (spnGender.getSelectedItemPosition() == 1) {
                    gender = "Male";
                } else {
                    gender = "Female";
                }
                if (spnCsuspect.getSelectedItemPosition() == 1) {
                    CSuspectStatus = "Low";
                } else if (spnCsuspect.getSelectedItemPosition() == 2) {
                    CSuspectStatus = "Medium";
                }else{
                    CSuspectStatus = "High";
                }
                Register();
            }
        }
    }

    public void Alert() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder
                .setMessage("Are You sure to Logout ?")
                .setCancelable(false);
            alertDialogBuilder.setPositiveButton("YES", (dialog, id) -> {
                L.logout(this);
                dialog.cancel();

            });
        alertDialogBuilder.setNegativeButton("NO", (dialog, id) -> {
            dialog.cancel();

        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();
    }

    private void Register() {

        Map<String, String> map = new HashMap<>();
        map.put("FName", L.getEditText(edtFname));
        map.put("LName", L.getEditText(edtLname));
        map.put("MobileNo", L.getEditText(edtMobile));
        map.put("EmailID", L.getEditText(edtemail));
        map.put("Area", L.getEditText(edtArea));
        map.put("Zipcode", L.getEditText(edtZipcode));
        map.put("VolunteerID", user.getVolunteerID());
        map.put("AmbulanceID", AMBULANCEID);
        map.put("DoctorID", DRID);
        map.put("Age", L.getEditText(edtAge));
        map.put("Gender",gender);
        map.put("CovidStaus", CSuspectStatus);
        map.put("Remarks", L.getEditText(edtRemark));
        map.put(Constant.DeviceID, L.getDeviceId(this));

        JSONObject obj=new JSONObject(map);
        Log.d(TAG, "reg: "+ map);
        Log.d(TAG, "reg: "+ obj.toString());

        Log.d(TAG, "reg: "+map);
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.SubmitForm(obj.toString()), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    assert data.body() != null;
                    JSONObject jsonResponse = new JSONObject(data.body());

                    if(jsonResponse.getJSONArray("records").getJSONObject(0).getString("Status").equalsIgnoreCase("Success")){
                        Intent intent = new Intent(this, FromFillActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        Toast.makeText(this, "Submitted Successfully", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this, ""+jsonResponse.getJSONArray("records").getJSONObject(0).getString("messgae"), Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }

    public void getDrList() {
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getDocterList(), (data) -> {
           // showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    DrList.clear();
                    DrList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray("records").toString(), DrModel.class));
                    drListAdapter.notifyDataSetChanged();
                    if (L.isNetworkAvailable(FromFillActivity.this))
                        getAmbulanceList();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    public void getAmbulanceList() {
        //showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getAmbulanceList(), (data) -> {
            showProgress(false);
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    ambulanceList.clear();
                    ambulanceList.addAll(LoganSquare.parseList(jsonResponse.getJSONArray("records").toString(), AmbulanceModel.class));
                    ambulanceListAdapter.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });
    }

    class SexAdapter extends BaseAdapter {
        List<SexModel> gender;
        LayoutInflater inflter;
        Context context;

        public SexAdapter(Context context, List<SexModel> sex) {
            this.context = context;
            this.gender = sex;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            int count = gender.size() + 1;
            return count > 0 ? count - 1 : count;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Gender");
                names.setTextColor(Color.GRAY);
            } else {
                SexModel locationModel = gender.get(position - 1);
                names.setText(locationModel.getValue());
                names.setTextColor(Color.parseColor("#304193"));

            }
            return convertView;
        }


    }

    class CSuspect extends BaseAdapter {
        List<SexModel> gender;
        LayoutInflater inflter;
        Context context;

        public CSuspect(Context context, List<SexModel> sex) {
            this.context = context;
            this.gender = sex;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            int count = gender.size() + 1;
            return count > 0 ? count - 1 : count;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Corona Suspect");
                names.setTextColor(Color.GRAY);
            } else {
                SexModel locationModel = gender.get(position - 1);
                names.setText(locationModel.getValue());
                names.setTextColor(Color.parseColor("#304193"));

            }
            return convertView;
        }


    }

    class DRListAdapter extends BaseAdapter {
        List<DrModel> drModel;
        LayoutInflater inflter;
        Context context;

        public DRListAdapter(Context context, List<DrModel> drModels) {
            this.context = context;
            this.drModel = drModels;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            int count = drModel.size() + 1;
            return count > 0 ? count - 1 : count;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Select Dr Name");
                names.setTextColor(Color.GRAY);
            } else {
                DrModel drModell = drModel.get(position - 1);
                names.setText(drModell.getFirstName()+" "+drModell.getLastName());
                names.setTextColor(Color.parseColor("#304193"));

            }
            return convertView;
        }


    }

    class AmbulanceListAdapter extends BaseAdapter {
        List<AmbulanceModel> ambulanceModel;
        LayoutInflater inflter;
        Context context;

        public AmbulanceListAdapter(Context context, List<AmbulanceModel> ambulanceModels) {
            this.context = context;
            this.ambulanceModel = ambulanceModels;
            inflter = (LayoutInflater.from(context));
        }

        @Override
        public int getCount() {
            int count = ambulanceModel.size() + 1;
            return count > 0 ? count - 1 : count;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = inflter.inflate(R.layout.custom_spinner_items, null);
            TextView names = convertView.findViewById(R.id.tv_spinner);
            if (position == 0) {
                names.setText("Select Ambulance No");
                names.setTextColor(Color.GRAY);
            } else {
                AmbulanceModel drModell = ambulanceModel.get(position - 1);
                names.setText(drModell.getAmbulanceNo());
                names.setTextColor(Color.parseColor("#304193"));
            }
            return convertView;
        }


    }
}
