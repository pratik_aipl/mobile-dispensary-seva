package com.addit.bjsmobiledispensaryseva.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.addit.bjsmobiledispensaryseva.BaseActivity;
import com.addit.bjsmobiledispensaryseva.BuildConfig;
import com.addit.bjsmobiledispensaryseva.R;
import com.addit.bjsmobiledispensaryseva.model.UserData;
import com.addit.bjsmobiledispensaryseva.network.NetworkRequest;
import com.addit.bjsmobiledispensaryseva.utils.Constant;
import com.addit.bjsmobiledispensaryseva.utils.L;
import com.addit.bjsmobiledispensaryseva.utils.Validation;
import com.bluelinelabs.logansquare.LoganSquare;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscription;


public class LoginActivity extends BaseActivity {

    private static final String TAG = "LoginActivity";
    @BindView(R.id.edt_mobile)
    EditText edtMobile;
    @BindView(R.id.edt_password)
    EditText edtPassword;
    @BindView(R.id.btn_login)
    Button btnLogin;

    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (BuildConfig.DEBUG) {
            edtMobile.setText("8433712275");
            edtPassword.setText("123456");
        }

    }

    @OnClick(R.id.btn_login)
    public void onViewClicked() {
        if (!Validation.isValidPhoneNumber(L.getEditText(edtMobile))) {
            edtMobile.setError("Please enter Mobile No");
        } else if (Validation.isEmpty(L.getEditText(edtPassword))) {
            edtPassword.setError("Please enter Password");
        } else {
            if (L.isNetworkAvailable(LoginActivity.this)){
                callLogin();
            }
        }
    }

    private void callLogin() {
        JSONObject map = new JSONObject();
        try {
            map.put(Constant.MOBILE, L.getEditText(edtMobile));
            map.put(Constant.Password, L.getEditText(edtPassword));
            map.put(Constant.DeviceID, L.getDeviceId(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        showProgress(true);
        subscription = NetworkRequest.performAsyncRequest(restApi.getLogin(map.toString()), (data) -> {
            showProgress(false);
            Toast.makeText(this, ""+data.code(), Toast.LENGTH_LONG).show();
            if (data.code() == 200) {
                try {
                    JSONObject jsonResponse = new JSONObject(data.body());
                    UserData user = LoganSquare.parse(jsonResponse.getJSONArray("records").getJSONObject(0).toString(), UserData.class);

                    if(jsonResponse.getJSONArray("records").getJSONObject(0).getString("Status").equalsIgnoreCase("Success")){
                        prefs.save(Constant.UserData, new Gson().toJson(user));
                        prefs.save(Constant.isLogin, true);

                        Intent intent = new Intent(this, FromFillActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP |
                                Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        Toast.makeText(this, "Login Successfully", Toast.LENGTH_SHORT).show();

                    }else{
                        Toast.makeText(this, ""+jsonResponse.getJSONArray("records").getJSONObject(0).getString("messgae"), Toast.LENGTH_SHORT).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                L.serviceStatusFalseProcess(this, data);
            }

        }, (e) -> {
            showProgress(false);
            e.printStackTrace();
        });

    }
}
