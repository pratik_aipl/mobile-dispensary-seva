package com.addit.bjsmobiledispensaryseva.utils;

public class Constant {


    public static String file = "file";

    public static String UserData = "UserData";
    public static String image = "image";
    public static String video = "video";
    public static String audio = "audio";
    public static String content = "content";
    public static String HTTP = "http";
    public static String HTTPS = "https";
    public static final String DeviceID = "device_token";
    public static boolean isAlertShow = false;
    public static String message = "message";
    public static String isLogin = "isLogin";
    public static String loginAuthToken = "loginAuthToken";
    public static String USERID = "UserId";
    public static String data = "data";


    public static String EMAILID = "EmailID";
    public static String USERName = "username";
    public static String SURNAME = "surname";
    public static String MOBILE = "MobileNo";
    public static String Password = "Password";
    public static String GENDER = "gender";
    public static String DOB = "dob";


    public static String cartData="cartData";
}
