package com.addit.bjsmobiledispensaryseva.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class UserData implements Serializable {

    @JsonField
    String Status;
    @JsonField
    String VolunteerID;
    @JsonField
    String isActive;
    @JsonField
    String FirstName;
     @JsonField
    String MobileNo;
    @JsonField
    String LastName;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getVolunteerID() {
        return VolunteerID;
    }

    public void setVolunteerID(String volunteerID) {
        VolunteerID = volunteerID;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }
}
