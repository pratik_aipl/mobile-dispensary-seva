package com.addit.bjsmobiledispensaryseva.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class SexModel implements Serializable {

    @JsonField
    String Value;
    @JsonField
    int ID;

    public SexModel() {
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String Value) {
        this.Value = Value;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public SexModel(String Value, int ID) {
        this.Value = Value;
        this.ID = ID;
    }
}
