package com.addit.bjsmobiledispensaryseva.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.io.Serializable;

@JsonObject
public class AmbulanceModel implements Serializable {

    @JsonField
    int AmbulanceID;
    @JsonField
    String FirstName;
    @JsonField
    String LastName;
    @JsonField
    String MobileNo;
    @JsonField
    String AmbulanceNo;

    public int getAmbulanceID() {
        return AmbulanceID;
    }

    public void setAmbulanceID(int ambulanceID) {
        AmbulanceID = ambulanceID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }

    public String getAmbulanceNo() {
        return AmbulanceNo;
    }

    public void setAmbulanceNo(String ambulanceNo) {
        AmbulanceNo = ambulanceNo;
    }
}
