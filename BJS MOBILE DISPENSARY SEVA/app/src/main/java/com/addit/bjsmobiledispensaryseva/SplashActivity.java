package com.addit.bjsmobiledispensaryseva;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.addit.bjsmobiledispensaryseva.activity.FromFillActivity;
import com.addit.bjsmobiledispensaryseva.activity.LoginActivity;
import com.addit.bjsmobiledispensaryseva.utils.Constant;
import com.addit.bjsmobiledispensaryseva.utils.L;

public class SplashActivity extends BaseActivity {

    private static final String TAG = "SplashActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        int SPLASH_TIME_OUT = 1000;
        new Handler().postDelayed(() -> {
            if (L.isNetworkAvailable(this)) {
                Log.d(TAG, "splash: "+prefs.getBoolean(Constant.isLogin, false));
                if (!prefs.getBoolean(Constant.isLogin, false)) {
                    Intent i;
                    i = new Intent(SplashActivity.this, LoginActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                } else {
                    Intent i;
                    i = new Intent(SplashActivity.this, FromFillActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    finish();
                }
            }

        }, SPLASH_TIME_OUT);
    }
}
